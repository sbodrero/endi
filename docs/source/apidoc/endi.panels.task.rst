endi.panels.task package
========================

Submodules
----------

endi.panels.task.file\_tab module
---------------------------------

.. automodule:: endi.panels.task.file_tab
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.task.html module
----------------------------

.. automodule:: endi.panels.task.html
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.task.pdf module
---------------------------

.. automodule:: endi.panels.task.pdf
   :members:
   :undoc-members:
   :show-inheritance:

endi.panels.task.task\_list module
----------------------------------

.. automodule:: endi.panels.task.task_list
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.panels.task
   :members:
   :undoc-members:
   :show-inheritance:
