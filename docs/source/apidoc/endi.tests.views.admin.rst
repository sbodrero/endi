endi.tests.views.admin package
==============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   endi.tests.views.admin.accompagnement
   endi.tests.views.admin.expense
   endi.tests.views.admin.main
   endi.tests.views.admin.sale
   endi.tests.views.admin.userdatas

Submodules
----------

endi.tests.views.admin.test\_tools module
-----------------------------------------

.. automodule:: endi.tests.views.admin.test_tools
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.tests.views.admin
   :members:
   :undoc-members:
   :show-inheritance:
