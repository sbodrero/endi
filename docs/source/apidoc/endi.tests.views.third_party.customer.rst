endi.tests.views.third\_party.customer package
==============================================

Submodules
----------

endi.tests.views.third\_party.customer.test\_controller module
--------------------------------------------------------------

.. automodule:: endi.tests.views.third_party.customer.test_controller
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.third\_party.customer.test\_customer module
------------------------------------------------------------

.. automodule:: endi.tests.views.third_party.customer.test_customer
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.third\_party.customer.test\_rest\_api module
-------------------------------------------------------------

.. automodule:: endi.tests.views.third_party.customer.test_rest_api
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.tests.views.third_party.customer
   :members:
   :undoc-members:
   :show-inheritance:
