endi.plugins.sap.forms package
==============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   endi.plugins.sap.forms.admin
   endi.plugins.sap.forms.tasks

Submodules
----------

endi.plugins.sap.forms.attestation module
-----------------------------------------

.. automodule:: endi.plugins.sap.forms.attestation
   :members:
   :undoc-members:
   :show-inheritance:

endi.plugins.sap.forms.nova module
----------------------------------

.. automodule:: endi.plugins.sap.forms.nova
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.plugins.sap.forms
   :members:
   :undoc-members:
   :show-inheritance:
