endi.tests.panels package
=========================

Submodules
----------

endi.tests.panels.test\_company module
--------------------------------------

.. automodule:: endi.tests.panels.test_company
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.panels.test\_menu module
-----------------------------------

.. automodule:: endi.tests.panels.test_menu
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.tests.panels
   :members:
   :undoc-members:
   :show-inheritance:
