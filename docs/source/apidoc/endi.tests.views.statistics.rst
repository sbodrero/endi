endi.tests.views.statistics package
===================================

Submodules
----------

endi.tests.views.statistics.test\_rest\_api module
--------------------------------------------------

.. automodule:: endi.tests.views.statistics.test_rest_api
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.tests.views.statistics
   :members:
   :undoc-members:
   :show-inheritance:
