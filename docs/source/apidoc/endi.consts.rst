endi.consts package
===================

Submodules
----------

endi.consts.insee\_countries module
-----------------------------------

.. automodule:: endi.consts.insee_countries
   :members:
   :undoc-members:
   :show-inheritance:

endi.consts.insee\_departments module
-------------------------------------

.. automodule:: endi.consts.insee_departments
   :members:
   :undoc-members:
   :show-inheritance:

endi.consts.street\_types module
--------------------------------

.. automodule:: endi.consts.street_types
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.consts
   :members:
   :undoc-members:
   :show-inheritance:
