endi.tests.views.project package
================================

Submodules
----------

endi.tests.views.project.test\_phase module
-------------------------------------------

.. automodule:: endi.tests.views.project.test_phase
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.project.test\_project module
---------------------------------------------

.. automodule:: endi.tests.views.project.test_project
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.views.project.test\_rest\_api module
-----------------------------------------------

.. automodule:: endi.tests.views.project.test_rest_api
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.tests.views.project
   :members:
   :undoc-members:
   :show-inheritance:
