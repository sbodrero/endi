endi.plugins.sap.views.admin package
====================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   endi.plugins.sap.views.admin.sap

Module contents
---------------

.. automodule:: endi.plugins.sap.views.admin
   :members:
   :undoc-members:
   :show-inheritance:
