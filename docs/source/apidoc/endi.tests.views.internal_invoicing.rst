endi.tests.views.internal\_invoicing package
============================================

Submodules
----------

endi.tests.views.internal\_invoicing.test\_views module
-------------------------------------------------------

.. automodule:: endi.tests.views.internal_invoicing.test_views
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.tests.views.internal_invoicing
   :members:
   :undoc-members:
   :show-inheritance:
