endi.tests.models.price\_study package
======================================

Submodules
----------

endi.tests.models.price\_study.test\_base module
------------------------------------------------

.. automodule:: endi.tests.models.price_study.test_base
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.models.price\_study.test\_chapter module
---------------------------------------------------

.. automodule:: endi.tests.models.price_study.test_chapter
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.models.price\_study.test\_discount module
----------------------------------------------------

.. automodule:: endi.tests.models.price_study.test_discount
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.models.price\_study.test\_price\_study module
--------------------------------------------------------

.. automodule:: endi.tests.models.price_study.test_price_study
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.models.price\_study.test\_product module
---------------------------------------------------

.. automodule:: endi.tests.models.price_study.test_product
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.models.price\_study.test\_services module
----------------------------------------------------

.. automodule:: endi.tests.models.price_study.test_services
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.models.price\_study.test\_work module
------------------------------------------------

.. automodule:: endi.tests.models.price_study.test_work
   :members:
   :undoc-members:
   :show-inheritance:

endi.tests.models.price\_study.test\_work\_item module
------------------------------------------------------

.. automodule:: endi.tests.models.price_study.test_work_item
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: endi.tests.models.price_study
   :members:
   :undoc-members:
   :show-inheritance:
