DEPRECATED Compiling my templates with handlebars.js
====================================================

.. note::
   We commit both templates and their compiled version in enDI git repo.

Handlebars.js is a javascript templating engine. It allows server-side template compilation.
Since we don't use nodejs as webserver, we need to precompile our templates.

For that, we need :

1. Install nodejs : https://github.com/joyent/node/wiki/Installing-Node.js-via-package-manager
2. Install build dependencies (incl. handlebars CLI)
   ::
      npm --prefix js_sources install

3. Compile templates after you modify it
   ::
      cd ..
      make js

Add your template in the handlebars directory. all the templates in this folder
are compiled to the same js file : template.js.
Maybe it should be splitted in several files one day.

It can be imported this way:

.. code-block:: python

    from endi.resources import templates

    def myview(request):
        templates.need()

Fanstatic will handle the import (and the requirements) for you.
Most of the time, the templates requirement is done through the declaration of a
new js ressource.
