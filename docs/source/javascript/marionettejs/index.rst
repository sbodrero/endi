Marionettejs
==============

Le code marionette est dans js_sources/
Il est buildé dans endi/static/js/build/

Bibliothèques
------------

* Backbone (http://backbonejs.org/)
* Marionette (http://marionettejs.com)
* Backbone.Validation (http://thedersen.com/projects/backbone-validation/)
* Jquery

Build
------

Installation des dépendances

    npm --prefix js_sources install

Build des fichiers de dev js (avec reload)::

    make devjs_watch

Sans reload::

    make devjs

Build des fichiers minifiés (prod) ::

    make prodjs

.. toctree::
    :maxdepth: 1

    formulaire.rst
