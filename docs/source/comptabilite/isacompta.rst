Export des écritures au format Isa Compta
=======================================================

enDI permet de configurer l'export des écritures au format compatible avec ce
qui est attendu par Isa compta.

Afin de configurer enDI pour utiliser ces modules, la configuration suivante
doit être ajoutée dans la section "[app:endi]" du fichier .ini

.. code-block::

    endi.services.treasury_invoice_producer=endi.compute.isacompta.compute.InvoiceProducer
    endi.services.treasury_internalinvoice_producer=endi.compute.isacompta.compute.InternalInvoiceProducer
    endi.services.treasury_invoice_writer=endi.export.isacompta.InvoiceWriter

    endi.services.treasury_payment_producer=endi.compute.isacompta.compute.PaymentProducer
    endi.services.treasury_internalpayment_producer=endi.compute.isacompta.compute.InternalPaymentProducer
    endi.services.treasury_payment_writer=endi.export.isacompta.PaymentWriter

    endi.services.treasury_expense_producer=endi.compute.isacompta.compute.ExpenseProducer
    endi.services.treasury_expense_writer=endi.export.isacompta.ExpenseWriter

    endi.services.treasury_supplier_invoice_producer=endi.compute.isacompta.compute.SupplierInvoiceProducer
    endi.services.treasury_internalsupplier_invoice_producer=endi.compute.isacompta.compute.InternalSupplierInvoiceProducer
    endi.services.treasury_supplier_invoice_writer=endi.export.isacompta.SupplierInvoiceWriter

Grand livre
------------

Un outil de traitement spécifique au grand livre issu d'isacompta permet
d'intégrer le grand livre directement dans endi.

Dans la configuration du service endi_celery ajouter

.. code-block::

    endi_celery.interfaces.IAccountingFileParser=endi_celery.parsers.isacompta.parser_factory
    endi_celery.interfaces.IAccountingOperationProducer=endi_celery.parsers.isacompta.producer_factory


Spécifications pour la configuration des imports IsaCompta
-------------------------------------------------------------

Les chapitres ci-dessous décrivent les formats des fichiers produits par enDI
pour Isa compta. Les imports dans IsaCompta doivent être configurés en fonction
des données ci-dessous.

Import Facture Vente
......................

- Entête dans le fichier d'import : Numéro de pièce
- Contient : Numéro de facture enDI


- Entête dans le fichier d'import : Code Journal
- Contient : code journal configuré dans l'administration d'endi


- Entête dans le fichier d'import : Date de pièce
- Contient : Date


- Entête dans le fichier d'import : N° compte général
- Contient : Compte général


- Entête dans le fichier d'import : Code tva
- Contient : Code TVA


- Entête dans le fichier d'import : Numéro de compte tiers
- Contient : Compte Tiers


- Entête dans le fichier d'import : Libellé pièce
- Contient : Nom du client


- Entête dans le fichier d'import : Libellé mouvement
- Contient : Libellé configuré dans l'administration d'endi


- Entête dans le fichier d'import : Montant débit
- Contient : Débit


- Entête dans le fichier d'import : Montant crédit
- Contient : Crédit


- Entête dans le fichier d'import : Numéro analytique
- Contient : Numéro analytique de l'enseigne

Import Notes de dépenses
...........................

- Entête dans le fichier d'import : Numéro de pièce
- Contient : Numéro du document dans enDI


- Entête dans le fichier d'import : Code Journal
- Contient : code journal configuré dans l'administration d'endi


- Entête dans le fichier d'import : Date de pièce
- Contient : Date


- Entête dans le fichier d'import : N° compte général
- Contient : Compte général


- Entête dans le fichier d'import : Code tva
- Contient : Code TVA


- Entête dans le fichier d'import : Numéro de compte tiers
- Contient : Compte Tiers


- Entête dans le fichier d'import : Libellé pièce
- Contient : Nom de l'entrepreneur


- Entête dans le fichier d'import : Libellé mouvement
- Contient : Libellé configuré dans l'administration d'endi


- Entête dans le fichier d'import : Montant débit
- Contient : Débit


- Entête dans le fichier d'import : Montant crédit
- Contient : Crédit


- Entête dans le fichier d'import : Numéro analytique
- Contient : Numéro analytique de l'enseigne


Import des encaissements
.........................

- Entête dans le fichier d'import : Référence
- Contient : reference


- Entête dans le fichier d'import : Code Journal
- Contient : code journal configuré dans l'administration d'endi


- Entête dans le fichier d'import : Date de pièce
- Contient : Date


- Entête dans le fichier d'import : N° compte général
- Contient : Compte général


- Entête dans le fichier d'import : Code tva
- Contient : Code TVA


- Entête dans le fichier d'import : Numéro de compte tiers
- Contient : Compte Tiers


- Entête dans le fichier d'import : Libellé pièce
- Contient : Nom du client


- Entête dans le fichier d'import : Libellé mouvement
- Contient : Libellé configuré dans l'administration d'endi


- Entête dans le fichier d'import : Montant débit
- Contient : Débit


- Entête dans le fichier d'import : Montant crédit
- Contient : Crédit


- Entête dans le fichier d'import : Numéro analytique
- Contient : Numéro analytique de l'enseigne


Import des factures fournisseurs
.................................

- Entête dans le fichier d'import : Numéro de pièce
- Contient : Numéro du document dans enDI


- Entête dans le fichier d'import : Code Journal
- Contient : code journal configuré dans l'administration d'endi


- Entête dans le fichier d'import : Date de pièce
- Contient : Date


- Entête dans le fichier d'import : N° compte général
- Contient : Compte général


- Entête dans le fichier d'import : Code tva
- Contient : Code TVA


- Entête dans le fichier d'import : Numéro de compte tiers
- Contient : Compte Tiers


- Entête dans le fichier d'import : Libellé pièce
- Contient : Nom du fournisseur


- Entête dans le fichier d'import : Libellé mouvement
- Contient : Libellé configuré dans l'administration d'endi


- Entête dans le fichier d'import : Montant débit
- Contient : Débit


- Entête dans le fichier d'import : Montant crédit
- Contient : Crédit


- Entête dans le fichier d'import : Numéro analytique
- Contient : Numéro analytique de l'enseigne
