Export des écritures au format Sage Generation expert
=======================================================

enDI permet de configurer l'export des écritures au format compatible avec ce
qui est attendu par Sage Generation Expert.

Afin de configurer enDI pour utiliser ces modules, la configuration suivante
doit être ajoutée dans la section "[app:endi]" du fichier .ini

.. code-block::

    endi.services.treasury_invoice_producer=endi.compute.sage_generation_expert.compute.InvoiceProducer
    endi.services.treasury_internalinvoice_producer=endi.compute.sage_generation_expert.compute.InternalInvoiceProducer
    endi.services.treasury_invoice_writer=endi.export.sage_generation_expert.InvoiceWriter

    endi.services.treasury_payment_producer=endi.compute.sage_generation_expert.compute.PaymentProducer
    endi.services.treasury_internalpayment_producer=endi.compute.sage_generation_expert.compute.InternalPaymentProducer
    endi.services.treasury_payment_writer=endi.export.sage_generation_expert.PaymentWriter

    endi.services.treasury_expense_producer=endi.compute.sage_generation_expert.compute.ExpenseProducer
    endi.services.treasury_expense_writer=endi.export.sage_generation_expert.ExpenseWriter

    endi.services.treasury_supplier_invoice_producer=endi.compute.sage_generation_expert.compute.SupplierInvoiceProducer
    endi.services.treasury_internalsupplier_invoice_producer=endi.compute.sage_generation_expert.compute.InternalSupplierInvoiceProducer
    endi.services.treasury_supplier_invoice_writer=endi.export.sage_generation_expert.SupplierInvoiceWriter

    endi.services.treasury_supplier_payment_producer=endi.compute.sage_generation_expert.compute.SupplierPaymentProducer
    endi.services.treasury_supplier_payment_user_producer=endi.compute.sage_generation_expert.compute.SupplierUserPaymentProducer
    endi.services.treasury_internalsupplier_payment_producer=endi.compute.sage_generation_expert.compute.InternalSupplierPaymentProducer
    endi.services.treasury_supplier_payment_writer=endi.export.sage_generation_expert.SupplierPaymentWriter

    endi.services.treasury_expense_payment_producer=endi.compute.sage_generation_expert.compute.ExpensePaymentProducer
    endi.services.treasury_expense_payment_writer=endi.export.sage_generation_expert.ExpensePaymentWriter
