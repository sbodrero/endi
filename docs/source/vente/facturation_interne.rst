Facturation interne
====================

Modèles spécifiques
--------------------

Les modèles suivants sont utilisés pour la facturation interne

* InternalEstimation
* InternalInvoice
* InternalPayment

* InternalSupplierOrder
* InternalSupplierInvoice
* InternalSupplierInvoiceSupplierPayment

Ils permettent de gérer la facturation entre

* un Customer de type_ 'internal' (créé par l'utilisateur)
* un Supplier de type_ 'internal' (créé automatiquement par enDI)



Configuration
----------------

Les éléments de configuration suivant doivent être définis lors de la mise en
place d'enDI :

- Configuration des comptes généraux des types de produits dédiés à la
  facturation interne
- Configuration des comptes généraux des types de dépenses dédiés aux
  factures fournisseurs internes.
- Configuration de la numérotation des factures internes.

Résumé
----------

- On crée un client interne à la CAE.

- pour chaque devis, on a une commande fournisseur (la validation de cette dernière vaut acceptation du devis)

- pour chaque facture on a une facture fournisseur

- pour chaque encaissement on a un paiement fournisseur

En terme comptable, ces éléments sont distincts des documents de vente/achat classiques (numérotation différente, code journaux et comptes généraux/analytiques différents).

En terme de gestion entrepreneuriale, dans enDI, elles sont comptabilisées dans le chiffre d'affaire de manière transparente (on travaille à mettre un peu en évidence la différenciation).

.. image:: ./_static/facturation_interne/principe.png
  :width: 100%
  :alt: Principe


Parcours utilisateur
---------------------

Composition d'un devis à destination d'une autre enseigne de la CAE
.....................................................................

.. image:: ./_static/facturation_interne/devis.png
  :width: 100%
  :alt: Devis interne


Commande
.........

En validant sa commande fournisseur interne, un entrepreneur valide le devis de
son fournisseur, cette étape est considérée comme contractuelle.

.. image:: ./_static/facturation_interne/commande.png
  :width: 100%
  :alt: Commande interne


Facturation interne
.......................

.. image:: ./_static/facturation_interne/facture.png
  :width: 100%
  :alt: Facturation interne

Facture Fournisseur interne
............................

.. image:: ./_static/facturation_interne/facture.png
  :width: 100%
  :alt: Facturation fournisseur interne

Encaissement
.............

.. image:: ./_static/facturation_interne/encaissement.png
  :width: 100%
  :alt: Encaissement


