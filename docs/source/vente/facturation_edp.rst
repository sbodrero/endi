Étude de prix (EDP)
====================

Les études de prix sont un mode de saisie spécifique aux devis/factures.

Les modèles des études de prix se trouvent dans endi.models.price_study.

Fonctionnement général
------------------------

À la création d'un devis dans un projet permettant les EDP, il est possible de sélectionner le mode de saisie EDP.

Le diagramme ci-dessous décrit la structure de modèles sous-jacentes.


.. image:: ./_static/etude_de_prix/diagramme.jpg
  :width: 100%
  :alt: Étude de prix
