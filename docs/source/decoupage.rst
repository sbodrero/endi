Découpage d'enDi : modules optionnels, plugins, dépôts…
========================================================

.. warning:: On parle ici de notions propres à enDi, aussi un « module » n'est
   pas nécessairement un « module python ».

Les dépôts git
---------------

Le logiciel enDi est découpé entre plusieurs dépôts git qui sont regroupés
dans le `namespace endi sur framagit`_.

La plupart du temps, pour le développement, seul le dépôt principal *endi/endi*
est nécessaire.
.. _`namespace endi sur framagit`:
   https://framagit.org/endi

Les modules optionnels
-----------------------

Les **modules optionnels** sont des parties d'enDi que l'on peut *désactiver*
pour une installation donnée. Par défaut tous les modules optionnels sont
activés.

Exemple : on peut désactiver le module formation si la CAE ne fait pas de
formation profesionnelle.

Ils se gèrent via la clef de configuration ``endi.modules`` dans le fichier .ini.

Par exemple, pour désactiver tous les modules optionnels (liste vide):

.. code-block:: ini

   endi.modules =

Ou bien par n'activer que l'accompagnement et les ateliers :


.. code-block:: ini

   endi.modules =
              endi.views.accompagnement
              endi.views.workshops


.. note:: Il est possible de trouver la liste des modules optionnels dans le
          fichier ``endi/__init__.py`` ; variable ``ENDI_OTHER_MODULES``.

Développement de modules optionnels
-----------------------------------

Outils :
~~~~~~~~

Il est parfois nécessaire de faire différement dans le code selon qu'un module
optionnel est activé ou non (ex: afficher tel ou tel lien) :

.. note::

   if request.has_module('endi.views.workshops'):
       ...



.. _plugins:

Les plugins
-----------

Les plugins vont modifier le comportement d'enDi sur plusieurs aspects :

- modifier les menus
- remplacer des routes/vues
- charger des données supplémentaires d'initialisation (`endi-admin syncdb`)
- ajouter des commandes (`endi-admin <nouvelle-commande>`)
- ajouter des modèles
- implémenter des `services` (cf :doc:`services` )

.. note::
   Les plugins existants à ce jour sont :

   - endi_payment⭐
   - endi_edocsafe⭐
   - endi_oidc⭐
   - endi.plugins.solo
   - endi.plugins.sap
   - endi.plugins.sap_urssaf3p

   ⭐ : dans un dépôt git à part.


Par défaut aucun plugin n'est activé, pour activer un plugin, il faut définir
l'entrée suivante dans le fichier .ini (qui contient donc une liste) :

.. code-block:: ini

   endi.includes =
           endi.plugins.solo


Les procédures pour activer un plugin peuvent être plus complexes selon le
plugin, auquel cas elles sont décrites dans sa documentation propre.

Développement de plugins
-------------------------

Outils :
~~~~~~~~

Lorsqu'il est trop compliqué de faire autrement, il est parfois nécessaire
d'avoir dans le code principal un code conditionellement à la présence ou non d'un plugin.

.. note::

   if request.has_plugin('endi_payment'):
       ...

Commandes endi-admin :
~~~~~~~~~~~~~~~~~~~~~~

Les plugins peuvent ajouter assez simplement des sous-commandes de *endi-admin* ; pour voir comment faire, regarder l'exemple de ce que fait le plugin sap_urssaf3p qui ajoute une commande ``endi-admin check_urssaf3p``.


Bonnes pratiques :
~~~~~~~~~~~~~~~~~~


Concernant les modèles:

- Pour ajouter des champs : éviter de surcharger/redéfinir des modèles/tables (SQLAlchemy) existants à
  l'intérieur d'un plugin… : modifier directement le modèle de base.
- Éviter de créer des modèles liés (ForeignKey, héritage…) aux autres modèles
  d'enDi.
- Importer inconditionellement (que le plugin soit actif ou non) les modèles apportés par les plugins
  depuis `endi/models/__init__.py`
- Identifier explicitement dans le code les champs/modèles ajoutés pour les
  besoins d'un plugin en particulier

Concernant les tests

- les tests sont lancés avec tous les plugins désactivés

.. note::

   Il n'y a pas actuellement de structure pour avoir des fichiers statiques
   (ex: JS/CSS) hors de l'application principale.
