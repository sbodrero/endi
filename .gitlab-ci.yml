# Tested target system is Debian Buster with MariaDB 10

variables:
  MYSQL_DATABASE: endi
  MYSQL_USER: endi
  MYSQL_PASSWORD: endi
  MYSQL_ROOT_PASSWORD: root
  TZ: "Europe/Paris"
  LC_ALL: fr_FR.UTF-8
  LANG: fr_FR.UTF-8
  LANGUAGE: fr_FR.UTF-8
  PYTHONUNBUFFERED: 0
  PYTHONIOENCODING: UTF-8
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

# services:
#     - mariadb:10.3

stages:
  - build
  - tests
  - tests_with_data

################ COMMON BEFORE #################

before_script:
    - echo 'fr_FR.UTF-8 UTF-8' >> /etc/locale.gen
    - apt-get update -qq
    - "apt-get install -qq
      curl locales libfreetype6
      python3-minimal python3-wheel python3-distutils python3-mysqldb
      shared-mime-info python3-cffi libcairo2 libpango-1.0-0 libpangocairo-1.0-0 libgdk-pixbuf2.0-0
      "


################# JOB TEMPLATES ################

.debian_10_env:
  image: debian:10
  services:
    - mariadb:10.3

.debian_11_env:
  image: debian:11
  services:
    - mariadb:10.5

.build_job:  # build python stuff
  script:
    - "apt-get install -qq
      build-essential
      python3-pip python3-venv
      libmariadb-dev-compat libmariadb-dev
      libffi-dev
      libjpeg-dev libssl-dev libfreetype6-dev libxml2-dev zlib1g-dev libxslt1-dev # other py lib deps
      "
    - python3 -m venv venv
    - source venv/bin/activate
    - pip install --upgrade setuptools pip
    - pip install -e .[dev]

  artifacts:
    expire_in: 1 day
    paths:
      - venv/
      - endi.egg-info/
  cache:
    key: py-dependencies-cache
    paths:
      - .cache/pip/


##################### JOBS ####################

build_js:
  stage: build
  extends:
    - .debian_11_env
  script:
    - "curl -sL https://deb.nodesource.com/setup_16.x | bash - && apt-get update -qq && apt-get install -qq nodejs make"
    - npm --cache .npm --prefix js_sources install
    - npm --cache .npm --prefix vue_sources install
    - make devjs prodjs devjs2 prodjs2

  artifacts:
    name: js_build-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA
    # Latest artifact of a given branch/tag is kept forever anyway
    expire_in: 1 day
    paths:
      - endi/static/js/build/
  cache:
    key: js-dependencies-cache
    paths:
      - .npm/
      - js_sources/node_modules/
      - vue_sources/node_modules/


build_debian_10:
  stage: build
  extends:
    - .debian_10_env
    - .build_job


unit_tests_debian_10:
  stage: tests
  needs:
    - build_js  # fanstatic needs the js artifact
    - build_debian_10
  extends:
    - .debian_10_env
  script:
    - source venv/bin/activate
    - py.test
    # Test only SAP-specific tests
    # trigger manually unit_tests_w_sap_plugin for complete tests w plugins
    - py.test --endi-plugins sap endi/tests/plugins/sap
    - py.test --endi-plugins sap sap_urssaf3p endi/tests/plugins



build_debian_11:
  stage: build
  extends:
    - .debian_11_env
    - .build_job


unit_tests_debian_11:
  stage: tests
  needs:
    - build_js  # fanstatic needs the js artifact
    - build_debian_11
  extends:
    - .debian_11_env
  script:
    - source venv/bin/activate
    - py.test
    # Test only SAP-specific tests
    # trigger manually unit_tests_w_sap_plugin for complete tests w plugins
    - py.test --endi-plugins sap endi/tests/plugins/sap
    - py.test --endi-plugins sap sap_urssaf3p endi/tests/plugins



linters:
  image: debian:11
  # Runs on debian 11 because debian 10 fails
  # on SyntaxError in pyyaml (Py3.6 related) at boussole run.
  stage: tests
  needs:
    - build_debian_11

  script:
    - source venv/bin/activate
    - black --check --diff .
    - apt-get install -qq make git
    - make css
    # Will fail if we forgot to run `make css`:
    - git diff --exit-code


import_reference_dump:
  stage: tests
  extends:
    - .debian_10_env
  needs:
    - build_js  # fanstatic needs the js artifact
    - build_debian_10

  script:
    - apt-get install -qq mariadb-client
    - source venv/bin/activate
    - endi-load-demo-data gitlab-ci.ini
    # Would fail in two cases:
    # 1. bug in a migration
    # 2. missing merge revision
    - endi-migrate gitlab-ci.ini upgrade
  after_script:
    - mysqldump -h mariadb --max-allowed-packet=1G -u$MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE > reference_dump_migrated.sql
  artifacts:
    expire_in: 1 day
    paths:
      - reference_dump_migrated.sql


test_anonymize_script:
  stage: tests_with_data
  # Ce job prend ~30min, il n'est pas raisonable de le lancer systématiquement
  when: manual
  extends:
    - .debian_10_env
  needs:
    - build_js  # fanstatic needs the js artifact
    - build_debian_10
    - import_reference_dump
  script:
    - source venv/bin/activate
    - apt-get install -qq mariadb-client
    - mysql -h mariadb -u$MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE < reference_dump_migrated.sql
    - endi-anonymize gitlab-ci.ini run


unit_tests_w_sap_plugin:
  stage: tests
  when: manual
  needs:
    - build_js  # fanstatic needs the js artifact
    - build_debian_10
  extends:
    - .debian_10_env
  script:
    # Full test suite with plugins enabled
    - source venv/bin/activate
    - py.test --endi-plugins sap endi
