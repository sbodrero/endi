import http from 'helpers/http'
import { getFormConfigStore } from './formConfig'
import getModelStore from './modelStore'

export const useProjectStore = getModelStore('project')
export const useProjectConfigStore = getFormConfigStore('project')
