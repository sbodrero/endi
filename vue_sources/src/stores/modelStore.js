import { createPinia, defineStore } from 'pinia'
import { getFormConfigStore } from './formConfig'
import http from 'helpers/http'
import { isProxy, toRaw } from 'vue'

const getModelStore = (name) =>
  defineStore({
    id: name,
    state: () => ({
      loading: true,
      item: {},
      collection: [],
      url: '',
      apiUrl: '',
      collectionUrl: '',
    }),
    actions: {
      /**
       *
       * @param {String} url : Url of the context (add or edit)
       */
      setUrl(apiUrl) {
        console.log('Setting apiUrl %s', apiUrl)
        this.apiUrl = apiUrl
      },
      setCollectionUrl(collectionUrl) {
        console.log('Setting collectionUrl %s', collectionUrl)
        this.collectionUrl = collectionUrl
      },
      /**
       * Load the current item
       *
       * @returns An instance of item
       */
      async load(item_id = null) {
        let url = this.apiUrl
        if (item_id) {
          if (!this.collectionUrl) {
            throw 'Collection url is not set on this store'
          }
          url = this.collectionUrl + item_id
        }
        console.log(`Loading the ${name} ${item_id} from pinia store ${url}`)
        return http.get(url).then((item) => {
          this.item = item
          this.loading = false
          return item
        })
      },
      async loadCollection(fields = [], related = []) {
        let params = fields.map((name) => ['fields', name])
        params = params.concat(related.map((name) => ['related', name]))
        const reqParams = new URLSearchParams(params)
        const url = `${this.collectionUrl}?${reqParams}`

        return http.get(url).then((collection) => {
          this.collection = collection
          return collection
        })
      },
      async edit(data) {
        console.log(`Modifying a ${name} from pinia store`)
        console.log(data)
        let url = this.apiUrl
        if (!url && this.collectionUrl) {
          url = this.collectionUrl + '/' + data.id
        }
        this.item = await http.put(url, data)
        return this.item
      },
      async create(data) {
        console.log(`Adding a ${name} from pinia store`)
        console.log(data)
        return http.post(this.apiUrl, data).then(
          (newItem) => {
            console.log('item added')
            this.collection.unshift(newItem)
            return newItem
          },
          (error) => {
            console.log('Forwarding error')
            console.log(error)
            return Promise.reject(error)
          }
        )
      },
      async save(values, item = {}) {
        if (item.id) {
          this.item = item
          return this.edit(values)
        } else {
          return this.create(values)
        }
      },
    },
    getters: {
      getByid(state) {
        return (id) => {
          id = parseInt(id)
          let result = state.collection.find((item) => item.id == id)
          if (isProxy(result)) {
            result = toRaw(result)
          }
          return result
        }
      },
      filterIn(state) {
        return (ids) => {
          return state.collection.filter((item) => ids.indexOf(item.id) != -1)
        }
      },
      getCollection(state) {
        return (filter) => state.collection.filter(filter)
      },
    },
  })
export default getModelStore
