import http from 'helpers/http'
import { getFormConfigStore } from './formConfig'
import getModelStore from './modelStore'

export const useTaskStore = getModelStore('task')
export const useTaskConfigStore = getFormConfigStore('task')
