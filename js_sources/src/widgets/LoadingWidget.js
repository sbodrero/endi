import Mn from 'backbone.marionette';
var template = require('./templates/LoadingWidget.mustache');
const LoadingWidget = Mn.View.extend({
    tagName: 'div',
    className: 'loading_box',
    template: template
});
export default LoadingWidget;
