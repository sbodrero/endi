import BaseFormWidget from './BaseFormWidget.js';
import {
    getOpt
} from '../tools.js';
import {
    formatDate
} from '../date.js';

var template = require('./templates/DateWidget.mustache');


const DateWidget = BaseFormWidget.extend({
    /*
    A jquery datepicker widget
    */
    template: template,
    ui: {
        input: "input"
    },
    events: {
        'blur @ui.input': "onBlur",
        'change @ui.input': "onChange",
        'click @ui.input': 'onClick',
        'keydown @ui.input': "onKeyDown",
    },
    onBlur: function () {
        let val = this.ui.input.val();
        this.triggerFinish(val);
    },
    onChange(event) {
        const value = this.ui.input.val();
        // Si c'est une action souris, on valide (c'est que l'on a sélectionné la valeur dans le calendrier)

        // NB : un cas reste problématique :
        // Je commence à saisir au clavier et je clique dans le calendrier (l'event ici ne se lancera pas)
        // Le click dans le calendrier ne lance pas d'event onclick
        if (this.isMouseChange) {
            this.triggerFinish(value);
        }
    },
    onClick() {
        // Lorsqu'on clique dans l'input on enregistre que c'est une action de la souris
        this.isKeyboardChange = false;
        this.isMouseChange = true;
    },
    onKeyDown() {
        // Si on tape au clavier, on enregistre que c'est une action clavier
        this.isKeyboardChange = true;
        this.isMouseChange = false;
    },
    getValue() {
        let value = getOpt(this, "date", this.getOption('value')); // FIXME
        return value;
    },
    templateContext: function () {
        /*
         * Give parameters for the templating context
         */
        // common widget parameters (see BaseFormWidget)
        let ctx = this.getCommonContext();

        let more_ctx = {
            value: this.getValue()
        };
        if (!getOpt(this, 'editable', true)) {
            more_ctx['date'] = formatDate(this.getValue());
        }
        more_ctx['erasable'] = false;
        if (!getOpt(this, 'required', false)) {
            if (this.getValue()) {
                more_ctx['erasable'] = true;
            }
        }

        return Object.assign(ctx, more_ctx);;
    }
});
export default DateWidget;
