import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

/**
 * A button sending status modifications to the facade radio channel
 */
const StatusButtonWidget = Mn.View.extend({
    tagName: 'div',
    template: require('./templates/StatusButtonWidget.mustache'),
    ui: {
        button: 'button'
    },
    events: {
        'click @ui.button': 'onClick'
    },
    onClick: function(event){
        this.triggerMethod('status:change', this.model);
        var facade = Radio.channel('facade');
        facade.trigger('status:change', this.model);
    }
});
export default StatusButtonWidget;
