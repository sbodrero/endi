import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

const Controller = Mn.Object.extend({
    initialize(options){
        this.facade = Radio.channel('facade');
        console.log("Controller.initialize");
        this.rootView = options['rootView'];
    },
});
export default Controller;
