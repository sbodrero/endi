import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
const template = require('./templates/HelpTextView.mustache');
const HelpTextView = Mn.View.extend({
    template: template,
    events: {
        'click button.compute': 'forceCompute',
    },
    triggers: {
        'click button.close': 'hide:help'
    },
    initialize() {
        this.config = Radio.channel('config');
        this.facade = Radio.channel('facade');
    },
    templateContext() {
        console.log("Template context")
        // Collect data sent to the template (model attributes are already transmitted)
        const result = {};
        const use_margin_rate = this.config.request('get:options', 'margin_rate_enabled');
        const computing_info = this.config.request('get:options', 'computing_info');

        let margin_rate = 0;
        if (use_margin_rate) {
            margin_rate = this.model.get('margin_rate') || 0;
        } else {
            margin_rate = computing_info['margin_rate'] || 0;
        }

        result['margin_rate'] = margin_rate;
        result['use_margin_rate'] = use_margin_rate;
        result['computing_info'] = computing_info;

        return result;
    },
    forceCompute() {
        console.log("Force compute")
        this.model.save({
            'mode': 'supplier_ht'
        });
    }
});
export default HelpTextView