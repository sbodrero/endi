import Mn from 'backbone.marionette';

import ActionToolbar from '../views/ActionToolbar';

const ToolbarAppClass = Mn.Application.extend({
  /*
  Base classe for Toolbar management
  */
  region: '#js_actions',
  getResumeView(actions) {
    return null; // default
  },
  getMainActions(actions) {
    return actions['main'];
  },
  getMoreActions(actions) {
    return actions['more'];
  },
  renderApp(actions) {
    const resume_view = this.getResumeView(actions);
    this.view = new ActionToolbar({
      main: this.getMainActions(actions),
      more: this.getMoreActions(actions),
      resume: resume_view
    });
    this.showView(this.view);
  },
  onStart(app, actions) {
    this.renderApp(actions);
  },
});
export default ToolbarAppClass;