import Bb from 'backbone';
import BaseModel from "base/models/BaseModel";

const StatusLogEntryModel = BaseModel.extend({
    'props': [
        'id',
        'datetime',
        'status',
        'label',
        'comment',
        'icon',
        'css_class',
        'user',
        'can_edit',
        'visibility',
        'pinned',
    ],
    defaults: {
        'label': '',
        'comment': '',
        'pinned': false,
    },
    cleanProps(attributes) {
        // Turn this method into no-op to avoid discarding null-attrs
        // Discarding null-attrs as cleanProps does prevents null-valued
        // attributed to be synced correctly from server.
        return attributes;
    },
});
export default StatusLogEntryModel;

