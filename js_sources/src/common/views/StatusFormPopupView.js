import Mn, { getOption } from 'backbone.marionette';
import ModalBehavior from '../../base/behaviors/ModalBehavior.js';
import { serializeForm, ajax_call, showLoader, getOpt } from '../../tools.js';


var template = require("./templates/StatusFormPopupView.mustache");

/** The modal to log a message about a new status
 */
const StatusFormPopupView = Mn.View.extend({
    id: 'status-msg-modal',
    template: template,
    ui: {
        'textarea': 'textarea',
        btn_cancel: '.cancel',
        submit: 'button[type=submit]',
        form: 'form'
    },
    behaviors: {
        modal: {
            behaviorClass: ModalBehavior
        }
    },
    events: {
        'click @ui.btn_cancel': 'destroy',
        'click @ui.submit': 'onSubmit'
    },
    initialize(options){
        this.action = options.action;
    },
    submitCallback: function(result){
    },
    onSubmit: function(event){
        event.preventDefault();
        let datas = serializeForm(this.getUI('form'));
        datas['submit'] = this.action.get('status');
        const url = this.action.get('url');
        showLoader();
        this.serverRequest = ajax_call(url, datas, "POST");
        this.serverRequest.then(
            this.submitCallback.bind(this)
        );
    },
    templateContext(){
        return this.action.attributes;
    }
});
export default StatusFormPopupView;
