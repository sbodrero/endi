import NodeFileViewerFactory from "../components/NodeFileViewerFactory";
import BaseViewerPopupView from "./BaseViewerPopupView";
import {getOpt} from "../../tools";

const NodeFileViewerPopupView = BaseViewerPopupView.extend({
    initialize(options) {
        this.file = options.file;
        this.popupTitle = getOpt(this, 'popupTitle', this.file.get('label'));
    },
    showPreview() {
        const view = NodeFileViewerFactory.getViewer(
            this.file,
            {title: this.file.get('name')},
        );
        this.showChildView('pdfpreview', view);
    },
});
export default NodeFileViewerPopupView;
