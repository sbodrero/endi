import Mn from 'backbone.marionette';
import ModalBehavior from '../../base/behaviors/ModalBehavior.js';
import LoadingWidget from "../../widgets/LoadingWidget";
var template = require("./templates/BaseViewerPopupView.mustache");

/**
 * Base class for viewer of document
 *
 * Implementors must :
 *  - set this.popupTitle variable
 */
const BaseViewerPopupView = Mn.View.extend({
    template: template,
    regions: {
        pdfpreview: ".pdfpreview",
        loader: ".loader",
    },
    behaviors: {
        modal: {
            behaviorClass: ModalBehavior
        }
    },
    childViewEvents: {
        "loader:start": "showLoader",
        "loader:stop": "hideLoader",
    },
    templateContext() {
      return {
        popupTitle: this.popupTitle,
      };
    },
    onRender() {
        this.showPreview();
    },
    showLoader() {
      const view = new LoadingWidget();
      this.showChildView("loader", view);
    },
    hideLoader() {
        this.getRegion('loader').empty();
    },
});
export default BaseViewerPopupView;
