import Mn from 'backbone.marionette';

const ErrorView = Mn.View.extend({
    tagName: 'div',
    className: 'alert alert-danger',
    template: require('./templates/ErrorView.mustache'),
    ui: {
        close: '.close',
    },
    events: {
        'click @ui.close': 'onClose',
    },
    onClose(){
        this.remove();
    },
    initialize(){
        this.errors = this.getOption('errors');
    },
    templateContext(){
        return {"errors": this.errors};
    }
});
export default ErrorView;
