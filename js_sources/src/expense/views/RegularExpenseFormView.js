import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';

import BaseExpenseFormView from './BaseExpenseFormView.js';
import SelectBusinessWidget from '../../widgets/SelectBusinessWidget.js';
import SelectWidget from '../../widgets/SelectWidget.js';
import InputWidget from '../../widgets/InputWidget.js';


const RegularExpenseFormView = BaseExpenseFormView.extend({
    childViewEvents: {
        'finish': 'onChildChange',
        'labelChange': 'onChildLabelChange',
    },
    onChildLabelChange(field_name, label) {
        if (['customer_id', 'business_id', 'project_id'].includes(field_name)) {
            const labelFieldName = field_name.replace('_id', '_label');
            this.model.set(labelFieldName, label);
        }
    },
    initialize(){
        BaseExpenseFormView.prototype.initialize.apply(this);

        var channel = Radio.channel('config');
        this.customers_url = channel.request(
            'get:options',
            'company_customers_url',
        );
        this.projects_url = channel.request(
            'get:options',
            'company_projects_url',
        );
        this.businesses_url = channel.request(
            'get:options',
            'company_businesses_url',
        );
    },
    getTypeOptions() {
        var channel = Radio.channel('config');
        return channel.request(
            'get:typeOptions',
            'regular'
        );
    },
    showCategorySelect(){
        const view = new SelectWidget({
            options: [{'value': 1, 'label': "Frais généraux"}, {'value': 2, 'label': "Achats clients"}],
            title: "Catégorie",
            field_name: "category",
            value: this.model.get('category'),
        });
        this.showChildView('category', view);
    },
    onRender(){
        BaseExpenseFormView.prototype.onRender.apply(this);
        let view;
        if (! this.getOption('add')){
            this.showCategorySelect();
        }
        if(this.model.get('category')!=1) {
            view = new SelectBusinessWidget({
                title: 'Rattacher la dépense à',
                customers_url: this.customers_url,
                projects_url: this.projects_url,
                businesses_url: this.businesses_url,
                customer_value: this.model.get('customer_id'),
                project_value: this.model.get('project_id'),
                business_value: this.model.get('business_id'),
                customer_label: this.model.get('customer_label'),
                project_label: this.model.get('project_label'),
                business_label: this.model.get('business_label'),
                required: false,
            });
            this.showChildView('business_link', view);
        }

        view = new InputWidget({
            value: this.model.get('description'),
            title: 'Description',
            field_name: 'description'
        });
        this.showChildView('description', view);
    },
});
export default RegularExpenseFormView;
