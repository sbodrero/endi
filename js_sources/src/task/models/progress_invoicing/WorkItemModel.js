import BaseModel from 'base/models/BaseModel'
import Radio from 'backbone.radio'
import {
    isCancelinvoice
} from './utils';

const WorkItemModel = BaseModel.extend({
    // Define the props that should be set on your model
    props: ["id", "percentage", "already_invoiced", 'work_id',
        'description',
        'unity',
        'quantity',
        "percentage",
        "already_invoiced", // avant cette facture
        // Dans l'affaire
        'total_ht_to_invoice',
        'total_tva_to_invoice',
        'total_ttc_to_invoice',
        // Après cette facture
        'percent_left',
        'total_ht_left',
        'total_ht',
        // 'tva_amount',
        // 'total_ttc',
        // "Acompte ?"
        "has_deposit"
    ],
    validation: function () {
        let range;
        const already_invoiced = this.get('already_invoiced') || 0;
        if (isCancelinvoice()) {
            range = [-1 * already_invoiced, 0]
        } else {
            range = [0, 100 - already_invoiced]
        }
        return {
            'percentage': {
                required: true,
                range: range,
                msg: `Veuillez saisir un pourcentage entre ${range[0]} et ${range[1]}`,
            }
        }
    },
    initialize: function () {
        WorkItemModel.__super__.initialize.apply(this, arguments);
        this.user_prefs = Radio.channel('user_preferences');
    },
    _price_label(key) {
        return this.user_prefs.request('formatAmount', this.get(key), false);
    },
    total_ht_to_invoice_label() {
        return this._price_label('total_ht_to_invoice')
    },
    tva_to_invoice_label() {
        return this._price_label('total_tva_to_invoice')
    },
    total_ttc_to_invoice_label() {
        return this._price_label('total_ttc_to_invoice')
    },
    total_ht_label() {
        return this._price_label('total_ht');
    },
    tva_label() {
        return this._price_label('tva');
    },
    total_ttc_label() {
        return this._price_label('ttc');
    }
})
export default WorkItemModel;