import Mn from 'backbone.marionette';
import PaymentLineView from './PaymentLineView.js';

const PaymentLineCollectionView = Mn.CollectionView.extend({
    tagName: 'tbody',
    className: 'paymentlines lines',
    childView: PaymentLineView,
    collectionEvents: {
        'change:reorder': 'render',
    },
    childViewTriggers: {
        'edit': 'line:edit',
        'delete': 'line:delete',
        'order:up': 'order:up',
        'order:down': 'order:down',
    },
    childViewOptions: function(model){
        let edit = this.getOption('edit');
        return {
            show_date: this.getOption('show_date'),
            edit: edit
        };
    },
    onChildviewOrderUp: function(childView){
        this.collection.moveUp(childView.model);
    },
    onChildviewOrderDown: function(childView){
        this.collection.moveDown(childView.model);
    },
});
export default PaymentLineCollectionView;
