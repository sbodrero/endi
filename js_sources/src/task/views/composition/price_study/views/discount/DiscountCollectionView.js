/*
 * Module name : DiscountCollectionView
 */
import Mn from 'backbone.marionette';
import Radio from 'backbone.radio';
import DiscountView from './DiscountView.js';
import DiscountEmptyView from './DiscountEmptyView.js';

const DiscountCollectionView = Mn.CollectionView.extend({
    tagName: 'tbody',
    className: "lines",
    childView: DiscountView,
    emptyView: DiscountEmptyView,
    collectionEvents: {
        'change:reorder': 'render',
        sync: 'render'
    },
    childViewTriggers: {
        'edit': 'edit',
        'delete': 'delete',
        'down': 'down',
        'up': 'up',
    },
});
export default DiscountCollectionView