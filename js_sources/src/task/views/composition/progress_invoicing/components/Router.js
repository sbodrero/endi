import AppRouter from 'marionette.approuter';

const Router = AppRouter.extend({
    appRoutes: {
        '': 'index',
        'index': 'index',
        'chapters/:id/products/:id': 'editProduct',
    }
});
export default Router;