export const capitalize = function(string){
    return string.charAt(0).toUpperCase() + string.substring(1).toLowerCase();
}

/** Escape HTML syntax from an HTML string
 *
 * Relies on the browser escaping rather than regex.
 *
 *
 * @param aString string to be escaped
 * @returns {String} escaped string
 */
function escapeHTML(aString) {
    const p = document.createElement('p');
    p.innerText = aString;
    return p.innerHTML;
}


/** Handlebars SafeString
 *
 * A safe string won't be HTML-escaped by handlebars.
 *
 * Equivalent to Handlebars SafeString class.
 *
 * Embeding handelbars for those few lines is an extra 70kio of minified js.
 */
export class SafeString {
    constructor(aString){
        this.string = aString;
    }
    toString(){
        return '' + this.string;
    }
    toHTML() {
        return this.toString();
    }
}


/** Replace carriage retuns (\n) by <br />
 *
 * Sanitize input to produce a result safe to code injection.
 */
export const addBr = function(aString) {
    const escapedString = escapeHTML(aString);
    return new SafeString(escapedString.replace(/(?:\r\n|\r|\n)/g, "<br />"));
};
