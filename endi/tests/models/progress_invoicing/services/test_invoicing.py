from endi.models.progress_invoicing.services.invoicing import (
    PlanService,
    ChapterService,
    ProductService,
    WorkService,
    WorkItemService,
)


class TestPlanService:
    pass


class TestChapterService:
    pass


class TestBaseProductService:
    pass


class TestProductService:
    pass


class TestWorkService:
    pass


class TestWorkItemService:
    pass
