import datetime
from endi.models.task.services import InvoiceService


class TestInvoiceService:
    def test_duplicate(
        self, get_csrf_request_with_db, full_invoice, user, customer, project, phase
    ):
        result = InvoiceService.duplicate(
            get_csrf_request_with_db(),
            full_invoice,
            user,
            project=project,
            customer=customer,
            phase=phase,
        )
        assert len(full_invoice.default_line_group.lines) == len(
            result.default_line_group.lines
        )
        assert len(full_invoice.discounts) == len(result.discounts)
        assert result.mentions == full_invoice.mentions

    def test_duplicate_financial_year(
        self, get_csrf_request_with_db, full_invoice, user, customer, project, phase
    ):
        full_invoice.financial_year = 2010
        result = InvoiceService.duplicate(
            get_csrf_request_with_db(),
            full_invoice,
            user,
            project=project,
            customer=customer,
            phase=phase,
        )
        assert result.financial_year == datetime.date.today().year
