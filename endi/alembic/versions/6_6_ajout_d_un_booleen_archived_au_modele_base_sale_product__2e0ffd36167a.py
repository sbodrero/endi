"""6.6 Ajout d'un booléen archived au modèle base_sale_product

Revision ID: 2e0ffd36167a
Revises: ea80e8805df8
Create Date: 2023-05-04 09:28:41.261607

"""

# revision identifiers, used by Alembic.
revision = "2e0ffd36167a"
down_revision = "ea80e8805df8"

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


def update_database_structure():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "base_sale_product",
        sa.Column("archived", sa.Boolean(), nullable=True),
    )
    # ### end Alembic commands ###


def migrate_datas():
    from alembic.context import get_bind
    from zope.sqlalchemy import mark_changed
    from endi_base.models.base import DBSESSION

    session = DBSESSION()
    conn = get_bind()

    conn.execute("UPDATE base_sale_product SET archived=0")

    mark_changed(session)
    session.flush()


def upgrade():
    update_database_structure()
    migrate_datas()


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("base_sale_product", "archived")
    # ### end Alembic commands ###
