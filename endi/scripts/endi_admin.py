"""
    Command to add an admin to enDI
"""
import argparse
import logging
import os
import base64
from typing import (
    Callable,
    Union,
    TypeVar,
)

from pyramid.paster import bootstrap
from sqlalchemy.exc import NoResultFound

from endi.scripts.utils import argparse_command
from endi_base.models.base import DBSESSION
from endi.forms.user.user import User
from endi.forms.user.login import Login


PWD_LENGTH = 10


def get_pwd() -> str:
    """
    Return a random password
    """
    return base64.b64encode(os.urandom(PWD_LENGTH)).decode()


class AbstractCommand:
    """
    Docstring will be used as CLI doc
    """

    name = None

    @classmethod
    def add_arguments(cls, parser: argparse.ArgumentParser) -> None:
        """Adds arguments to the subcmd parser

        no-op if no special arguments is used for the subcmd

        :param parser: the sub-command parser already added to main CMD subparsers
        """
        return None

    @staticmethod
    def __call__(arguments: argparse.Namespace, env: dict):
        raise NotImplementedError


CommandClassType = TypeVar("CommandClassType", bound=AbstractCommand)


class UserAddcommand(AbstractCommand):
    """
    Add a user in the database
    """

    name = "useradd"

    @classmethod
    def add_arguments(cls, parser) -> None:
        parser.add_argument("--username", default="admin")
        parser.add_argument("--password", "--pwd", default=get_pwd)
        parser.add_argument("--firstname", default="Admin")
        parser.add_argument("--lastname", default="enDi")
        parser.add_argument("--email", default="admin@example.com")
        parser.add_argument("--group", default=None)

    @staticmethod
    def __call__(arguments: argparse.Namespace, env):
        is_autogen_password = not isinstance(arguments.password, str)

        if is_autogen_password:
            # callable for default
            password = arguments.password()
        else:
            password = arguments.password

        logger = logging.getLogger(__name__)
        logger.debug("Adding a user {0}".format(arguments.username))

        login = Login(login=arguments.username)
        login.set_password(password)

        if arguments.group:
            try:
                login.groups.append(arguments.group)
            except NoResultFound:
                print(
                    (
                        """
    
    ERROR : group %s doesn't exist, did you launched the syncdb command :
    
        endi-admin <fichier.ini> syncdb
                    """
                        % (arguments.group,)
                    )
                )
                return

        db = DBSESSION()
        db.add(login)
        db.flush()

        user = User(
            login=login,
            firstname=arguments.firstname,
            lastname=arguments.lastname,
            email=arguments.email,
        )
        db.add(user)
        db.flush()
        print(
            (
                """
        User Account created :
              ID        : {0.id}
              Login     : {0.login.login}
              Firstname : {0.firstname}
              Lastname  : {0.lastname}
              Email     : {0.email}
              Groups    : {0.login.groups}
              """.format(
                    user
                )
            )
        )

        if is_autogen_password:
            print(
                (
                    """
              Password  : {0}""".format(
                        password
                    )
                )
            )

        logger.debug("-> Done")
        return user


class TestMailCommand(AbstractCommand):
    """
    Test tool for mail sending
    """

    name = "testmail"

    @classmethod
    def add_arguments(cls, parser) -> None:
        parser.add_argument("--to", default="contact@endi.coop")

    @staticmethod
    def __call__(arguments: argparse.Namespace, env):
        from endi_base.mail import send_mail

        request = env["request"]
        subject = "Test d'envoi de mail"
        body = """Il semble que le test d'envoi de mail a réussi.
        Ce test a été réalisé depuis le script endi-admin
    
        Bonne et belle journée !!!"""

        send_mail(request, [arguments.to], body, subject)


class SyncdbCommand(AbstractCommand):
    """
    Populate the database with the initial datas
    """

    name = "syncdb"

    @classmethod
    def add_arguments(cls, parser) -> None:
        parser.add_argument("--pkg", default="endi")

    @staticmethod
    def __call__(arguments, env):
        from endi.models.populate import populate_database

        logger = logging.getLogger(__name__)

        populate_database()
        from endi.scripts.endi_migrate import (
            fetch_head_command,
            is_alembic_initialized,
        )

        # Do not fetch current head on an existing instance (risk of skipping migrations):
        if not is_alembic_initialized(arguments.pkg):
            logger.info(
                "Fetching current alembic head, skipping all migrations (new installation)"
            )
            fetch_head_command(arguments.pkg)


class ResizeHeadersCommand(AbstractCommand):
    """
    bulk resize company header files to limit pdf size
    """

    name = "resize_headers"

    @classmethod
    def add_arguments(cls, parser) -> None:
        parser.add_argument("--limit", default=None)
        parser.add_argument("--offset", default=None)

    @staticmethod
    def __call__(arguments: argparse.Namespace, env: dict):
        from sqlalchemy import distinct
        from endi.models.files import File
        from endi.models.company import Company
        from endi.forms.company import HEADER_RESIZER

        session = DBSESSION()
        file_id_query = session.query(distinct(Company.header_id))
        if arguments.limit:
            file_id_query = file_id_query.limit(arguments.limit)
            if arguments.offset:
                file_id_query = file_id_query.offset(arguments.offset)

        header_ids = [i[0] for i in file_id_query]
        header_files = File.query().filter(File.id.in_(header_ids))
        for header_file in header_files:
            file_datas = header_file.data_obj
            if file_datas:
                print(("Resizing header with id : {}".format(header_file.id)))
                header_file.data = HEADER_RESIZER.complete(file_datas)
                session.merge(header_file)


class EndiAdminCommandsRegistry:
    BASE_COMMANDS = [
        UserAddcommand,
        TestMailCommand,
        SyncdbCommand,
        ResizeHeadersCommand,
    ]

    EXTRA_COMMANDS = []

    @classmethod
    def _get_all_commands(cls):
        return cls.EXTRA_COMMANDS + cls.BASE_COMMANDS

    @classmethod
    def add_function(cls, command_class: CommandClassType) -> None:
        cls.EXTRA_COMMANDS.append(command_class)

    @classmethod
    def get_command(cls, name: str) -> Union[Callable[[dict, dict], None], None]:
        """
        :param name: the command name
        :returns None if no known command is mentioned in arguments
        """
        for cmd in cls._get_all_commands():
            if cmd.name == name:
                return cmd()

    @classmethod
    def get_argument_parser(cls):
        parser = argparse.ArgumentParser(description="enDi administration tool")
        parser.add_argument("config_uri")

        subparsers = parser.add_subparsers(dest="subcommand", required=True)
        for cmd in cls._get_all_commands():
            description = cmd.__doc__.strip()
            subparser = subparsers.add_parser(cmd.name, description=description)
            cmd.add_arguments(subparser)

        return parser


def admin_entry_point():
    def callback(arguments, env):
        func = EndiAdminCommandsRegistry.get_command(arguments.subcommand)
        return func(arguments, env)

    try:
        import sys

        try:
            # We need to bootstrap the app in order to collect commands registered by
            # plugins. Even required to build the doc.
            ini_file = sys.argv[1]
            bootstrap(ini_file)
        except IndexError:
            print("No ini file specified, plugin commands won't be listed")
        parser = EndiAdminCommandsRegistry.get_argument_parser()
        return argparse_command(callback, parser)
    finally:
        pass
