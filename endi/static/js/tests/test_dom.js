
module("DOM Manipulation tools");
test("Test Id manipulation", function(){
  equal(getIdFromTagId("abcdefgh_", "abcdefgh_2"), 2);
  });
test("Del function", function(){
  var line = "<div id='test'>Test</div>";
  $('#qunit-fixture').html($(line));
  delNode('test');
  var test = $('#test');
  equal($('#test').length, 0);
});
