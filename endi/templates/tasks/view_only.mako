<%doc>
    Base template for task readonly display
</%doc>
<%inherit file="${context['main_template'].uri}" />

<%block name="headtitle">
${request.layout_manager.render_panel('task_title_panel', title=title)}
</%block>
<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
    ${request.layout_manager.render_panel('action_buttons', links=main_actions)}
    ${request.layout_manager.render_panel('action_buttons', links=more_actions)}
</div>
</%block>

<%block name='content'>
<div>
	% if hasattr(next, 'panel_heading'):
		<h2>${next.panel_heading()}</h2>
	% endif
	% if hasattr(next, 'before_task_tabs'):
		${next.before_task_tabs()}
	% endif
	<ul class="nav nav-tabs icon-tabs" role="tablist">
		<li role="presentation" class="active">
			<a href="#summary" aria-controls="summary" id="summary-tabtitle" role='tab' data-toggle='tab'>
				<span class='icon'>${api.icon('info-circle')}</span>
				<span>Général</span>
			</a>
		</li>
		<li role="presentation">
			<a href="#documents" aria-controls="documents" id="documents-tabtitle" role='tab' data-toggle='tab'>
				<span class='icon'>${api.icon('eye')}</span>
				<span>Prévisualisation</span>
			</a>
		</li>
		% if hasattr(next, 'moretabs'):
			${next.moretabs()}
		% endif
		% if api.has_permission('view.file'):
			<li role="presentation">
				<a href="#attached_files" aria-controls="attached_files" id="attached_files-tabtitle" role='tab' data-toggle='tab' title="Fichiers attachés" aria-label="Fichiers attachés">
					<span class='icon'>${api.icon('paperclip')}</span>
					<span>Fichiers</span>
					% if request.context.children:
						<span class="counter">(${len(request.context.children)})</span>
					% endif
				</a>
			</li>
		% endif
	</ul>
	<div class='tab-content'>
		<div role='tabpanel' class="tab-pane active row" id="summary" aria-labelledby="summary-tabtitle">
			% if hasattr(next, 'before_summary'):
				<div>${next.before_summary()}</div>
			% endif
			% if indicators:
				<div class="separate_bottom">
					<h3>Indicateurs</h3>
					<div class="table_container">
						<table>
							<thead>
								<tr>
									<th scope="col" class="col_status" title="Statut"><span class="screen-reader-text">Statut</span></th>
									<th scope="col" class="col_status" title="Domaine d’application de l’indicateur"><span class="screen-reader-text">Domaine d’application de l’indicateur</span></th>
									<th scope="col" class="col_text">Indicateur</th>
									<th scope="col" class="col_text">Fichier</th>
									<th scope="col" class="col_actions width_two" title="Actions"><span class="screen-reader-text">Actions</span></th>
								</tr>
							</thead>
							<tbody>
								${request.layout_manager.render_panel('sale_file_requirements', file_requirements=indicators)}
							</tbody>
						</table>
					</div>
				</div>
			% endif
			<div class="layout flex two_cols separate_bottom">
				<div>
					<h3>Informations générales</h3>
					<dl class='dl-horizontal'>
						<dt>Statut</dt>
						<dd>
							<span class="icon status  ${task.global_status}">
								${api.icon(api.status_icon(request.context))}
							</span>
							${api.format_status(request.context)}
						</dd>
						% if task.business_type and task.business_type.name != 'default':
						% if task.business_id is not None:
							<dt>Affaire</dt>
							<dd><a href="${request.route_path('/businesses/{id}/overview', id=task.business_id)}">${task.business_type.label} : ${task.business.name}</a></dd>
						% else:
							<dt>Affaire de type</dt>
							<dd>${task.business_type.label}</dd>
						% endif
						% endif
						<dt>Nom du document</dt>
						<dd>${request.context.name}</dd>
						<dt>Date</dt>
						<dd>${api.format_date(request.context.date)}</dd>
						<dt>Client</dt>
						<dd>
							<a href="${request.route_path('customer', id=request.context.customer.id)}" title="Voir la fiche du client" aria-label="Voir la fiche du client">
								<span class='icon'>${api.icon('address-card')}</span>${request.context.customer.label}
								% if request.context.customer.code:
									<small>(${request.context.customer.code})</small>
								% endif
							</a>
							% if request.context.customer.email:
							<br />
							<a href="mailto:${request.context.customer.email}" title="Envoyer un mail au client" aria-label="Envoyer un mail au client">
								<span class='icon'>${api.icon('envelope')}</span>${request.context.customer.email}
							</a>
							% endif
						</dd>
						<dt>Montant HT</dt>
						<dd>${api.format_amount(request.context.ht, precision=5)}&nbsp;€</dd>
						<dt>TVA</dt>
						<dd>${api.format_amount(request.context.tva, precision=5)}&nbsp;€ </dd>
						<dt>TTC</dt>
						<dd>${api.format_amount(request.context.ttc, precision=5)}&nbsp;€</dd>
					</dl>
					% if hasattr(next, 'after_summary'):
						${next.after_summary()}
					% endif
				</div>
                <!-- will get replaced by backbone -->
            	<div class="status_history"></div>
			</div>
		</div>

		<div role="tabpanel" class="tab-pane row" id="documents" aria-labelledby="documents-tabtitle">
			<div class="separate_block limited_width width60 content_padding task_view tab_preview">
				${request.layout_manager.render_panel('task_html', context=task)}
			</div>
		</div>

		% if hasattr(next, 'moretabs_datas'):
			${next.moretabs_datas()}
		% endif

		<!-- attached files tab -->
		% if api.has_permission('view.file'):
			<% title = "Liste des fichiers attachés"  %>
		   ${request.layout_manager.render_panel('task_file_tab', title=title)}
		% endif

	</div>
</div>
</%block>


<%block name='footerjs'>
    AppOption = {};
    % for key, value in js_app_options.items():
        AppOption["${key}"] = "${value}"
    % endfor;
</%block>
