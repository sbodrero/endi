from pyramid.httpexceptions import (
    HTTPFound,
    HTTPNotFound,
)

from endi.forms.training.bpf import (
    get_business_bpf_edit_schema,
    get_year_from_request,
)
from endi.models.services.bpf import BPFService
from endi.models.task.invoice import (
    get_invoice_years,
)
from endi.resources import bpf_js
from endi.utils.colanderalchemy import patched_objectify
from endi.utils.widgets import (
    ButtonDropDownMenu,
    ViewLink,
)
from endi.events.business import BpfDataModified
from endi.utils.datetimes import get_current_year
from endi.views.training.routes import (
    BUSINESS_BPF_DATA_FORM_URL,
    BUSINESS_BPF_DATA_DELETE_URL,
    BUSINESS_BPF_DATA_LIST_URL,
)
from endi.views.business.business import BusinessOverviewView
from endi.views import (
    BaseEditView,
    BaseView,
    DeleteView,
    TreeMixin,
)


class BusinessBPFMixin:
    @property
    def bpf_datas(self):
        return self.current_business.bpf_datas

    @property
    def current_business(self):
        return self.context

    @property
    def new_bpf_years(self):
        """
        The financial years eligible for new bpf data (with no existing bpf)

        :yield: <year>, <bpf year url>
        """
        existing_bpf_years = set(i.financial_year for i in self.bpf_datas)
        for year in get_invoice_years():
            if year not in existing_bpf_years:
                yield year

    @property
    def new_bpfdata_menu(self):
        menu = ButtonDropDownMenu()
        menu.name = "Renseigner le BPF pour une année supplémentaire"
        menu.icon = "plus"

        for year in self.new_bpf_years:
            link = ViewLink(
                year,
                path=BUSINESS_BPF_DATA_FORM_URL,
                id=self.current_business.id,
                year=year,
            )
            menu.add(link)
        return menu


class BusinessBPFDataListView(TreeMixin, BusinessBPFMixin, BaseView):
    route_name = BUSINESS_BPF_DATA_LIST_URL
    title = "Données BPF"

    @property
    def tree_url(self):
        return self.request.route_path(
            self.route_name,
            id=self.current_business.id,
        )

    @property
    def bpf_datas_links(self):
        for bpf_data in self.bpf_datas:
            form_link = self.request.route_path(
                BUSINESS_BPF_DATA_FORM_URL,
                id=self.current_business.id,
                year=bpf_data.financial_year,
            )
            delete_link = self.request.route_path(
                BUSINESS_BPF_DATA_DELETE_URL,
                id=self.current_business.id,
                year=bpf_data.financial_year,
            )
            yield [bpf_data, form_link, delete_link]

    def __call__(self):
        self.populate_navigation()

        # More than 1 bpf data : offer choice
        if len(self.bpf_datas) > 1:
            return dict(
                current_business=self.current_business,
                bpf_datas=self.bpf_datas,
                bpf_datas_links=self.bpf_datas_links,
                title=self.title,
                new_bpfdata_menu=self.new_bpfdata_menu,
            )
        else:
            try:
                year = self.bpf_datas[0].financial_year
            except IndexError:
                year = get_current_year()

            return HTTPFound(
                self.request.route_path(
                    BUSINESS_BPF_DATA_FORM_URL,
                    id=self.current_business.id,
                    year=year,
                )
            )


class BusinessBPFDataEditView(BusinessBPFMixin, TreeMixin, BaseEditView):
    """Create+Edit view for BusinessBPFData Model

    As there is maximum one BusinessBPFData per Business, this is a single
    view, with a « create or update » logic.
    """

    schema = get_business_bpf_edit_schema()
    route_name = BUSINESS_BPF_DATA_FORM_URL
    add_template_vars = [
        "new_bpfdata_menu",
        "other_bpf_datas",
        "is_creation_form",
        "context_model",
        "delete_link",
    ]

    @property
    def title(self):
        return get_year_from_request(self.request)

    @property
    def is_creation_form(self):
        return self.get_context_model().id is None

    @property
    def other_bpf_datas(self):
        bpf_data_couples = []
        for bpf_data in self.bpf_datas:
            if bpf_data != self.get_context_model():
                link = self.request.route_path(
                    BUSINESS_BPF_DATA_FORM_URL,
                    id=bpf_data.business.id,
                    year=bpf_data.financial_year,
                )
                bpf_data_couples.append([bpf_data, link])
        return bpf_data_couples

    @property
    def context_model(self):
        return self.get_context_model()

    @property
    def delete_link(self):
        return self.request.route_path(
            BUSINESS_BPF_DATA_DELETE_URL,
            id=self.bpf_data.business.id,
            year=self.bpf_data.financial_year,
        )

    def get_context_model(self):
        try:
            return self.bpf_data  # cached
        except AttributeError:
            self.bpf_data = BPFService.get_or_create(
                self.context.id,
                get_year_from_request(self.request),
            )
            # We do not want to save anything now.
            if self.bpf_data.id is None:
                self.dbsession.expunge(self.bpf_data)
        return self.bpf_data

    def before(self, form):
        self.populate_navigation()
        bpf_js.need()
        return BaseEditView.before(self, form)

    def merge_appstruct(self, appstruct, model):
        # Workaround ColanderAlchemy bug #101 (FlushError)
        # https://github.com/stefanofontanelli/ColanderAlchemy/issues/101
        # A PR is ongoing, if merged/released, that workaround should be removed
        # https://github.com/stefanofontanelli/ColanderAlchemy/pull/103
        model = patched_objectify(self.schema, appstruct, model)
        return model

    def submit_success(self, appstruct):
        if len(appstruct["trainee_types"]) == 1:
            appstruct["trainee_types"][0]["headcount"] = appstruct["headcount"]
            appstruct["trainee_types"][0]["total_hours"] = appstruct["total_hours"]
        return super(BusinessBPFDataEditView, self).submit_success(appstruct)

    def on_edit(self, appstruct):
        self.request.registry.notify(
            BpfDataModified(self.request, self.get_context_model().business_id)
        )

    def redirect(self):
        return HTTPFound(
            self.request.route_path(
                BUSINESS_BPF_DATA_LIST_URL,
                id=self.get_context_model().business.id,
            )
        )


class BusinessBPFDeleteView(DeleteView):
    def redirect(self):
        return HTTPFound(
            self.request.route_path(
                BUSINESS_BPF_DATA_LIST_URL,
                id=self._business.id,
            )
        )

    def on_before_delete(self):
        # The context is Business, which is not what we want to delete…
        self._bpf_data = BPFService.get(
            business_id=self.context.id,
            financial_year=get_year_from_request(self.request),
        )
        if self._bpf_data is None:
            raise HTTPNotFound()
        else:
            self._business = self.context
            self.context = self._bpf_data

    def on_delete(self):
        # Restore context
        self.context = self._business
        self.request.registry.notify(BpfDataModified(self.request, self._business.id))


def includeme(config):
    config.add_view(
        BusinessBPFDeleteView,
        route_name=BUSINESS_BPF_DATA_DELETE_URL,
        permission="edit.bpf",
        request_method="POST",
        require_csrf=True,
    )

    config.add_tree_view(
        BusinessBPFDataEditView,
        parent=BusinessBPFDataListView,
        renderer="endi:templates/training/bpf/business_bpf_data_form.mako",
        permission="edit.bpf",
        layout="business",
    )

    config.add_tree_view(
        BusinessBPFDataListView,
        parent=BusinessOverviewView,
        renderer="endi:templates/training/bpf/business_bpf_data_list.mako",
        permission="edit.bpf",
        layout="business",
    )
