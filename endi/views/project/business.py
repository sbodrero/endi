"""
Views related to Businesses
"""
from sqlalchemy import (
    distinct,
)
from endi_base.models.base import DBSESSION
from endi.models.project.business import Business
from endi.utils.widgets import Link

from endi.views import (
    BaseListView,
    TreeMixin,
)
from endi.views.company.routes import (
    COMPANY_ESTIMATION_ADD_ROUTE,
    COMPANY_INVOICE_ADD_ROUTE,
)
from endi.views.project.routes import (
    PROJECT_ITEM_BUSINESS_ROUTE,
    PROJECT_ITEM_INVOICE_ROUTE,
)
from endi.views.business.routes import (
    BUSINESS_ITEM_ROUTE,
)
from endi.views.project.lists import ProjectListView
from endi.views.project.project import remember_navigation_history


class ProjectBusinessListView(BaseListView, TreeMixin):
    """
    View listing businesses
    """

    add_template_vars = (
        "stream_actions",
        "add_estimation_url",
        "add_invoice_url",
        "tva_display_mode",
        "tva_on_margin",
    )
    schema = None
    sort_columns = {
        "created_at": Business.created_at,
        "name": Business.name,
    }
    default_sort = "name"
    default_direction = "asc"
    route_name = PROJECT_ITEM_BUSINESS_ROUTE
    item_route_name = BUSINESS_ITEM_ROUTE

    @property
    def title(self):
        return "Affaires du dossier {0}".format(self.current().name)

    def current_id(self):
        if hasattr(self.context, "project_id"):
            return self.context.project_id
        else:
            return self.context.id

    @property
    def tree_url(self):
        return self.request.route_path(self.route_name, id=self.current_id())

    @property
    def add_estimation_url(self):
        return self.request.route_path(
            COMPANY_ESTIMATION_ADD_ROUTE,
            id=self.context.company_id,
            _query={"project_id": self.context.id},
        )

    @property
    def add_invoice_url(self):
        return self.request.route_path(
            COMPANY_INVOICE_ADD_ROUTE,
            id=self.context.company_id,
            _query={"project_id": self.context.id},
        )

    @property
    def tva_on_margin(self) -> bool:
        # Note that in case of a folder mixing tva on margin / regular tva, we would return True.
        # This is considered acceptable edge case.
        return self.context.project_type.default_business_type.tva_on_margin

    @property
    def tva_display_mode(self):
        if self.context.mode == "ttc" or self.tva_on_margin:
            return "ttc"
        else:
            return "ht"

    def current(self):
        if hasattr(self.context, "project"):
            return self.context.project
        else:
            return self.context

    def query(self):
        remember_navigation_history(self.request, self.context.id)
        query = DBSESSION().query(distinct(Business.id), Business)
        query = query.filter_by(project_id=self.current().id)
        return query

    def filter_closed(self, query, appstruct):
        self.populate_navigation()
        closed = appstruct.get("closed", True)
        if not closed:
            query = query.filter_by(closed=False)
        return query

    def stream_actions(self, item):
        yield Link(self._get_item_url(item), "Voir/Modifier", icon="pen", css="icon")


def includeme(config):
    config.add_tree_view(
        ProjectBusinessListView,
        parent=ProjectListView,
        renderer="endi:templates/project/business_list.mako",
        permission="list.businesses",
        layout="project",
    )
