import colanderalchemy
from endi.models.project.project import ProjectCustomer, Project
from endi.forms.project import get_edit_project_schema, get_full_add_project_schema
from endi.utils.controller import BaseAddEditController, RelatedAttrManager


class ProjectRelatedAttrManager(RelatedAttrManager):
    def _add_related_customer_ids(self, project: Project, project_dict: dict) -> dict:
        result = self.dbsession.query(ProjectCustomer.c.customer_id).filter(
            ProjectCustomer.c.project_id == project.id
        )
        project_dict["customer_ids"] = [p[0] for p in result]
        return project_dict

    def _add_related_phases(self, project: Project, project_dict: dict) -> dict:
        project_dict["phases"] = [
            phase.__json__(self.request) for phase in project.phases
        ]
        return project_dict

    def _add_related_business_types(self, project: Project, project_dict: dict) -> dict:
        project_dict["business_types"] = [
            btype.__json__(self.request)
            for btype in project.get_all_business_types(self.request)
        ]
        if project.project_type.default_business_type:
            project_dict[
                "default_business_type_id"
            ] = project.project_type.default_business_type.id
        return project_dict


class ProjectAddEditController(BaseAddEditController):
    related_manager_factory = ProjectRelatedAttrManager

    def get_schema(self, submitted: dict) -> colanderalchemy.SQLAlchemySchemaNode:
        if "schema" not in self._cache:
            if not self.edit:
                self._cache["schema"] = get_full_add_project_schema()
            else:
                self._cache["schema"] = get_edit_project_schema()
        return self._cache["schema"]

    def after_add_edit(self, project: Project, edit: bool, attributes: dict) -> Project:
        """
        Post formatting Hook

        :param project: Current project (added/edited)

        :param edit: Is it an edit form ?

        :param attributes: Validated attributes sent to this view

        :return: The modified project
        """
        if not edit:
            project.company = self.context

        return project
