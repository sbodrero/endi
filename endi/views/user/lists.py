"""
User and user datas listing views
"""
import logging
from sqlalchemy import (
    or_,
    distinct,
)
from endi_base.models.base import DBSESSION
from endi.models.company import (
    Company,
    CompanyActivity,
)
from endi.models.user.user import User
from endi.models.user.login import Login
from endi.models.user.group import Group
from endi.forms.user.user import get_list_schema
from endi.utils.widgets import Link
from endi.views import BaseListView


logger = logging.getLogger(__name__)


class BaseUserListView(BaseListView):
    """
    Base list for the User model
    Provide :

        The base User class query
        The filtering on the search field
        The filtering on the company activity_id

    add filters to specify more specific list views (e.g: trainers, users with
    account ...)
    """

    title = "Tous les comptes"
    schema = None
    sort_columns = dict(
        name=User.lastname,
        email=User.email,
    )
    add_template_vars = ("stream_actions",)

    def query(self):
        query = DBSESSION().query(distinct(User.id), User)
        query = query.outerjoin(User.companies)
        return query.filter(User.id > 0)

    def filter_name_search(self, query, appstruct):
        search = appstruct["search"]
        if search:
            query = query.filter(
                or_(
                    User.lastname.like("%" + search + "%"),
                    User.firstname.like("%" + search + "%"),
                    User.companies.any(Company.name.like("%" + search + "%")),
                    User.companies.any(Company.goal.like("%" + search + "%")),
                    User.login.has(Login.login.like("%" + search + "%")),
                )
            )

        return query

    def filter_activity_id(self, query, appstruct):
        activity_id = appstruct.get("activity_id")
        if activity_id:
            query = query.filter(
                User.companies.any(
                    Company.activities.any(CompanyActivity.id == activity_id)
                )
            )
        return query

    def filter_user_group(self, query, appstruct):
        group_id = appstruct.get("group_id")
        if group_id:
            query = query.filter(User.login.has(Login.groups.any(Group.id == group_id)))
        return query

    def stream_actions(self, item):
        """
        Compile the action description for the given item
        """
        yield Link(
            self.request.route_path(
                "/users/{id}",
                id=item.id,
            ),
            "",
            title="Voir le compte",
            icon="pen",
            css="icon only",
        )
        if self.request.has_module("accompagnement") and self.request.has_permission(
            "add.activity"
        ):
            yield Link(
                self.request.route_path(
                    "activities", _query={"action": "new", "user_id": item.id}
                ),
                "",
                title="Nouveau rendez-vous",
                icon="calendar-plus",
                css="icon only",
            )


class GeneralAccountList(BaseUserListView):
    """
    List the User models with Login attached to them
    """

    title = "Annuaire des utilisateurs"
    schema = get_list_schema()
    sort_columns = dict(
        name=User.lastname,
        email=User.email,
    )

    def filter_login_filter(self, query, appstruct):
        """
        Filter the list on accounts with login only
        """
        query = query.join(User.login)
        login_filter = appstruct.get("login_filter", "active_login")
        if login_filter == "active_login":
            query = query.filter(Login.active == True)  # NOQA : E712
        elif login_filter == "unactive_login":
            query = query.filter(Login.active == False)  # NOQA : E712
        return query


def includeme(config):
    """
    Pyramid module entry point

    :param obj config: The pyramid configuration object
    """
    config.add_view(
        GeneralAccountList,
        route_name="/users",
        renderer="/user/lists.mako",
        permission="visit",
    )
