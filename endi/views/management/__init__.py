def includeme(config):
    config.include(".companies")
    config.include(".kms")
    config.include(".payments")
