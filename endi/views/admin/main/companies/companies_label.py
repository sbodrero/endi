import os

from pyramid.httpexceptions import HTTPFound

from endi_base.models.base import DBSESSION
from endi.views.admin.tools import BaseConfigView
from endi.forms.admin import get_config_schema
from endi.views.admin.main.companies import (
    MainCompaniesIndex,
    COMPANIES_INDEX_URL,
)

COMPANIES_LABEL_ROUTE = os.path.join(COMPANIES_INDEX_URL, "companies_label")


class CompaniesLabelView(BaseConfigView):
    title = "Désignation des enseignes"
    description = "Afficher le nom de l'entrepreneur à la suite du nom de l'enseigne"
    route_name = COMPANIES_LABEL_ROUTE

    keys = ("companies_label_add_user_name",)
    schema = get_config_schema(keys)


def includeme(config):
    config.add_route(COMPANIES_LABEL_ROUTE, COMPANIES_LABEL_ROUTE),
    config.add_admin_view(
        CompaniesLabelView,
        parent=MainCompaniesIndex,
    )
