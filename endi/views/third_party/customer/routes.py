import os

COMPANY_CUSTOMERS = "/companies/{id}/customers"
CUSTOMER_ITEM = "customer"
COMPANY_CUSTOMERS_ADD = os.path.join(COMPANY_CUSTOMERS, "add")
API_COMPANY_CUSTOMERS = "/api/v1/companies/{id}/customers"
CUSTOMER_REST = "/api/v1/customers/{id}"


def includeme(config):
    config.add_route(
        API_COMPANY_CUSTOMERS,
        API_COMPANY_CUSTOMERS,
        traverse="/companies/{id}",
    )

    config.add_route(
        CUSTOMER_ITEM,
        r"/customers/{id:\d+}",
        traverse="/customers/{id}",
    )
    config.add_route(
        CUSTOMER_REST,
        CUSTOMER_REST,
        traverse="/customers/{id}",
    )

    config.add_route(
        "/api/v1/customers/{id}/statuslogentries",
        r"/api/v1/customers/{id:\d+}/statuslogentries",
        traverse="/customers/{id}",
    )

    config.add_route(
        "/api/v1/customers/{eid}/statuslogentries/{id}",
        r"/api/v1/customers/{eid:\d+}/statuslogentries/{id:\d+}",
        traverse="/statuslogentries/{id}",
    )

    for route in (COMPANY_CUSTOMERS, COMPANY_CUSTOMERS_ADD):
        config.add_route(route, route, traverse="/companies/{id}")

    config.add_route(
        "customers.csv", r"/company/{id:\d+}/customers.csv", traverse="/companies/{id}"
    )
