"""
Attached files related views
"""
from endi.models.task import Task

from endi.views.business.routes import (
    BUSINESS_ITEM_FILE_ROUTE,
    BUSINESS_ITEM_ADD_FILE_ROUTE,
)
from endi.views.project.business import ProjectBusinessListView
from endi.views.project.files import (
    ProjectFileAddView,
    ProjectFilesView,
)
from endi.views.business.business import BusinessOverviewView


class BusinessFileAddView(ProjectFileAddView):
    route_name = BUSINESS_ITEM_ADD_FILE_ROUTE


class BusinessFilesView(ProjectFilesView):
    route_name = BUSINESS_ITEM_FILE_ROUTE
    add_route = BUSINESS_ITEM_ADD_FILE_ROUTE
    help_message = """
    Liste des fichiers attachés à l'affaire courante ou à un des documents
    qui la composent."""

    @property
    def title(self):
        return "Fichiers attachés à l'affaire {0}".format(self.context.name)

    def collect_parent_ids(self):
        ids = [
            i[0]
            for i in self.request.dbsession.query(Task.id).filter_by(
                business_id=self.context.id
            )
        ]

        ids.append(self.context.id)
        return ids


def includeme(config):
    config.add_tree_view(
        BusinessFileAddView,
        parent=BusinessOverviewView,
        permission="add.file",
        layout="default",
        renderer="endi:templates/base/formpage.mako",
    )
    config.add_tree_view(
        BusinessFilesView,
        parent=ProjectBusinessListView,
        permission="list.files",
        renderer="endi:templates/business/files.mako",
        layout="business",
    )
