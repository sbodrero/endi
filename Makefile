JS_SOURCES_DIR=js_sources
VUE_JS_SOURCES_DIR=vue_sources
JS_TEMPLATE_MAIN_FILE=endi/static/js/template.js
JS_TEMPLATE_DEST=endi/static/js/templates
JS_TEMPLATES_SOURCE=$(JS_SOURCES_DIR)/src/handlebars
DIRECTORIES=$(shell cd $(JS_TEMPLATES_SOURCE) && find *  -maxdepth 1 -type d)
export PATH := $(JS_SOURCES_DIR)/node_modules/handlebars/bin:$(PATH)


SASSC=$(shell which pysassc)
CSS_SOURCES=css_sources/
CSS_DEST=endi/static/css/
CSSDIRECTORIES=$(shell cd $(CSS_SOURCES) && find * -maxdepth 0 -type d -not -path "_*")

# Used to build templates used in inline javascript stuff (not webpacked)
js:
	echo $(PATH)
	handlebars $(JS_TEMPLATES_SOURCE)/*.mustache  -f $(JS_TEMPLATE_MAIN_FILE)
	for dir in $(DIRECTORIES);do \
		handlebars $(JS_TEMPLATES_SOURCE)/$$dir/*.mustache -f $(JS_TEMPLATE_DEST)/$$dir.js; \
	done

black:
	black .

# build js with webpack
prodjs:  # *.minjs
	cd $(JS_SOURCES_DIR) && npm run prod

devjs:  # *.js
	cd $(JS_SOURCES_DIR) && npm run dev

devjs_watch:  # watch files in js_sources and run build automatically on file edit
	cd $(JS_SOURCES_DIR) && npm run devwatch

# build js with webpack
prodjs2:  # *.minjs
	cd $(VUE_JS_SOURCES_DIR) && npm run prod

devjs2:  # *.js
	cd $(VUE_JS_SOURCES_DIR) && npm run dev

devjs2_watch:  # watch files in js_sources and run build automatically on file edit
	cd $(VUE_JS_SOURCES_DIR) && npm run devwatch

# build css with libsassc
css:
	cd ${CSS_SOURCES} && boussole compile

css_watch:
	cd ${CSS_SOURCES} && boussole compile && boussole watch

dev_serve:
	pserve --reload development.ini

npm_upgrade:  # Upgrade JS dependencies if required
	npm --prefix $(JS_SOURCES_DIR) install
	npm --prefix $(VUE_JS_SOURCES_DIR) install

pip_dev_upgrade:  # Upgrade Python dependencies if required (and version number)
	pip install -e .[dev]

postupgrade_dev: npm_upgrade pip_dev_upgrade prodjs devjs prodjs2 devjs2
	endi-migrate development.ini upgrade  # run alembic migrations
	endi-admin development.ini syncdb  # initialize some database objects
