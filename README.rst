==========
enDI
==========

Un progiciel de gestion pour les CAE (Coopérative d'activité et d'emploi),
les collectifs d'entrepreneurs indépendants.

Licence
-------

Ceci est un logiciel libre, pour les conditions d'accès, d'utilisation,
de copie et d'exploitation, voir LICENSE.txt

Nouvelles fonctionnalités/Anomalies
-----------------------------------

Site officiel : http://endi.coop

L'essentiel du développement est réalisé sur financement de Coopérer pour
entreprendre. Si vous souhaitez plus d'information, une offre d'hébergement,
vous pouvez les contacter info@cooperer.coop

Si vous rencontrez un bogue, ou avez une idée de fonctionnalité, il est possible
de signaler cela aux développeurs directement ou en utilisant le système de
tickets de GitLab (framagit).
Exception : pour les bogues de sécurité, merci d'écrire un courriel à votre administrateur.

Instructions pour l'installation du logiciel (en environnement de prod)
-----------------------------------------------------------------------

Installation des paquets (nécessaire pour l'installation dans un environnement virtuel):

Sous Debian/Ubuntu:


.. note::

   On utilise le dépôt nodesource pour avoir une version adaptée de nodejs. Si on ne souhaite pas faire ça, il est possible à la place d'`utiliser docker-compose`_ pour compiler le javascript.

.. _utiliser docker: https://endi.readthedocs.io/fr/latest/javascript/build_docker.html

.. code-block:: console

    curl -fsSL https://deb.nodesource.com/setup_16.x | bash - &&\

    apt install virtualenvwrapper libmariadb-dev libmariadb-dev-compat npm build-essential libjpeg-dev libfreetype6 libfreetype6-dev libssl-dev libxml2-dev zlib1g-dev python3-mysqldb redis-server libxslt1-dev python3-pip fonts-open-sans


Sous Fedora:

.. code-block:: console

    dnf install virtualenvwrapper mardiadb-devel python-devel libxslt-devel libxml2-devel libtiff-devel libjpeg-devel libzip-devel freetype-devel lcms2-devel libwebp-devel tcl-devel tk-devel gcc redis-server open-sans-fonts

Création d'un environnement virtuel Python.

.. code-block:: console

    mkvirtualenv endi -p python3

Téléchargement et installation de l'application

.. code-block:: console

    git clone https://framagit.org/endi/endi.git
    cd endi
    python setup.py install
    cp development.ini.sample development.ini

Téléchargement des dépendances JS (requiert nodejs >= 16.x)

.. code-block:: console

    npm --prefix js_sources install
    npm --prefix vue_sources install

Compilation du code JS

.. code-block:: console

    make prodjs devjs
    make prodjs2 devjs2

Éditer le fichier development.ini et configurer votre logiciel (Accès à la base
de données, différents répertoires de ressources statiques ...).

Initialiser la base de données

.. code-block:: console

    endi-admin development.ini syncdb

Si vous utilisez un paquet tiers utilisant d'autres base de données (comme
endi_payment en mode production)

.. code-block:: console

    endi-migrate app.ini syncdb --pkg=endi_payment

.. note::

    L'application synchronise alors automatiquement les modèles de données.

Puis créer un compte administrateur

.. code-block:: console

    endi-admin development.ini useradd [--user=<user>] [--pwd=<password>] [--firstname=<firstname>] [--lastname=<lastname>] [--group=<group>] [--email=<email>]

N.B : pour un administrateur, préciser

.. code-block:: console

    --group=admin


Puis lancer l'application web

.. code-block:: console

    pserve development.ini

Installation (en environnement de dév)
--------------------------------------

.. note::

   Vous avez besoin d'une base de données mariadb, soit vous en avez une en
   local, soit cf section « vagrant » pour en installer une dans la bonne
   version sans toucher à votre système.


Installer les dépendendances système (cf ligne ``apt`` ou ``dnf``, selon votre
OS, dans la partie concernant l'installation en prod).

Ensuite, installez votre enDI de dév avec les commandes suivantes :

.. code-block:: console

    sudo apt install […] (idem à la section concernant la prod)
    git clone https://framagit.org/endi/endi.git
    cd endi
    cp development.ini.sample development.ini
    make postupgrade_dev

Il est possible de charger une base de données de démonstration complète
(écrase votre BDD endi si elle existe) avec :

.. code-block::

   endi-load-demo-data development.ini
   endi-migrate development.ini upgrade

Exécution des tâches asynchrones
---------------------------------

Un service de tâches asynchrones basé sur celery et redis est en charge de
l'exécution des tâches les plus longues.

Voir :
https://framagit.org/endi/endi_celery

pour plus d'informations.

Mise à jour (en environnement de prod)
--------------------------------------

La mise à jour d'enDI s'effectue en plusieurs temps (il est préférable de
sauvegarder vos données avant de lancer les commandes suivantes)

Mise à jour des dépendances python et du numéro de version

.. code-block:: console

    pip install .


Mise à jour de la structure de données

.. code-block:: console

    endi-migrate app.ini upgrade

Si vous utilisez un paquet tiers utilisant d'autres base de données (comme
endi_payment en mode production)

.. code-block:: console

    endi-migrate app.ini upgrade --pkg=endi_payment

Configuration des données par défaut dans la base de données

.. code-block:: console

    endi-admin app.ini syncdb

Met à jour les dépendances JS

.. code-block:: console

    npm --prefix js_sources install

Compile le JavaScript :

    make prodjs


Mise à jour/changement de branche (environnement de dév)
---------------------------------------------------------
Ces instructions sont à suivre une fois à jour sur la branche git
souhaitée. Elles sont sans risque : au pire elles ne feront rien si tout est
déjà à jour.

La commande suivante devrait s'occuper de tout

.. code-block:: console

    make postupgrade_dev


.. note::

    Le fichier Makefile est commenté si besoin de plus d'infos/détails sur ce
    que fait cette commande.


Standards de codage Python
^^^^^^^^^^^^^^^^^^^^^^^^^^

Le code enDI doit être formatté en respectant la pep8_.

À cette fin il est recommandé d'utiliser un analyseur de code comme flake8_.

En complément, afin d'assurer une uniformisation dans la mise en forme du code,
l'outil de formattage de code black_ doit être utilisé pour le développement.

Il peut être configuré `au niveau de votre éditeur`_ (le plus confortable) et/ou en
pre-commit.

.. _pep8: https://www.python.org/dev/peps/pep-0008/
.. _flake8: https://flake8.pycqa.org/en/latest/
.. _black: https://black.readthedocs.io/en/stable/index.html
.. _au niveau de votre éditeur: https://black.readthedocs.io/en/stable/integrations/editors.html

.. note::

   Pour activer le pre-commit hook (une fois pour toutes) : depuis le venv :

   ``pre-commit install``

   Ensuite, à chaque commit, lorsque votre code n'est pas formatté correctement
   selon black le reformatera au moment du commit **et fera échouer
   le commit**. Il faudra alors ajouter (``git add``) les modifications
   apportées par black et commiter à nouveau.

Il est également possible de lancer black manuellement sur l'ensemble du projet :

.. code-block:: console

   make black

(si vous n'utilisez pas black en local, l'intégration continue vous le rappelera 😁)

Base de données avec Vagrant
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pour héberger la base de données dans une machine virtuelle jetable et
reproductible sans toucher à la machine hôte, une configuration Vagrant est
disponible. Pour l'utiliser :

.. code-block:: console

    apt install vagrant

Et pour lancer cette machine :

.. code-block:: console

    vagrant up

Un serveur MariaDB est alors installé et configuré (port local 13306 de l'hôte
local, base: endi, login: endi, password: endi).

Des configurations adaptées à vagrant sont commentées dans ``test.ini.sample`` et
``developement.ini.sample``.

Au besoin, la base peut être remise à zéro avec :

.. code-block:: console

    vagrant provision


Tests
------

Copier et personnaliser le fichier de configuration

.. code-block:: console

    cp test.ini.sample test.ini

Lancer les tests

.. code-block:: console

   py.test endi/tests

Documentation utilisateur
--------------------------

Le guide d'utilisation se trouve à cette adresse :
https://doc.endi.coop

*****

*****

:Ce projet est testé avec: `BrowserStack <https://www.browserstack.com/>`_
